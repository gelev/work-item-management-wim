package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;

import java.util.List;

public class ShowAllTeamsCommand implements Command {
    private static final String NO_TEAMS_MESSAGE = "No teams found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ShowAllTeamsCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        if (engine.getAllMembers().keySet().toString().isEmpty()) {
            throw new IllegalArgumentException(NO_TEAMS_MESSAGE);
        }

        System.out.print("All teams: ");
        return engine.getAllTeams().keySet().toString().replace("[", "").replace("]", "");
    }
}
