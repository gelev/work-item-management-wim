package system.wim.models.contracts;


import java.util.List;

public interface Member {
    String getName();

    List<WorkItem> getWorkItems();

    List<String> getActivityHistory();

    void addToActivityHistory(String message);

    void addWorkItem(WorkItem workItem);

    void removeWorkItem(WorkItem workItem);
}
