package system.wim.models.modelsImpl;

import system.wim.models.contracts.Bug;
import system.wim.models.contracts.History;
import system.wim.models.contracts.Member;
import system.wim.models.enums.BugSeverity;
import system.wim.models.enums.BugStatus;
import system.wim.models.enums.Priority;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BugImpl extends TaskImpl implements Bug {
    private final static String WORK_ITEM_TYPE = "Bug ----";
    private static final String STEP_TO_REPRODUCE_NULL_OR_EMPTY_MESSAGE = "Steps to reproduce cannot be empty! You must add commands, separated by ', ' ";

    private List<String> stepsToReproduce;
    private BugSeverity severity;
    private BugStatus status;

    public BugImpl(String title, String description, String stepsToReproduceInp, Priority priority, Member memberAssignee, BugSeverity severity, BugStatus status) {
        super(title, description, priority, memberAssignee);
        setSeverity(severity);
        this.status = status;
        stepsToReproduce = new ArrayList<>();
        stepsToReproduce = stepConvert(stepsToReproduceInp);
    }

    @Override
    public String getStepsToReproduce() {
        return String.join(" -> ", stepsToReproduce);
    }

    @Override
    public BugSeverity getSeverity() {
        return this.severity;
    }

    @Override
    public BugStatus getBugStatus() {
        return this.status;
    }

    @Override
    public void changePriorityOfBug(Priority priority) {
        History history = new HistoryImpl("bug priority", getPriority().toString(), priority.toString());
        super.addHistory(history);
        setPriority(priority);
    }

    @Override
    public void changeSeverityOfBug(BugSeverity severity) {
        History history = new HistoryImpl("bug severity", getSeverity().toString(), severity.toString());
        super.addHistory(history);
        setSeverity(severity);
    }

    @Override
    public void changeStatusOfBug(BugStatus status) {
        History history = new HistoryImpl("bug status", getBugStatus().toString(), status.toString());
        super.addHistory(history);
        setStatus(status);
    }

    @Override
    public String printWorkItemType() {
        return WORK_ITEM_TYPE;
    }

    protected String printDetails() {
        StringBuilder sb = new StringBuilder(super.printDetails());
        sb.append("Bug severity: ").append(getSeverity().toString())
                .append(System.lineSeparator())
                .append("Bug status: ").append(getBugStatus().toString())
                .append(System.lineSeparator())
                .append("Steps to reproduce: ").append(getStepsToReproduce()).append(System.lineSeparator())
        .append("~~~~~~~~~~~~~~~~~~~~~~").append(System.lineSeparator());

        return sb.toString();
    }

    @Override
    protected void setPriority(Priority priority) {
        super.setPriority(priority);
    }

    private void setStatus(BugStatus status) {
        this.status = status;
    }

    private void setSeverity(BugSeverity severity) {
        this.severity = severity;
    }

    private List<String> stepConvert(String stepsToReproduceInp) {
        if (stepsToReproduceInp==null || stepsToReproduceInp.isEmpty()){
            throw new IllegalArgumentException(STEP_TO_REPRODUCE_NULL_OR_EMPTY_MESSAGE);
        }
        String[] step =stepsToReproduceInp.split(", ");
        return  Arrays.asList(step);
    }
}

