package system.wim.commands.creation;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Board;
import system.wim.models.contracts.Feedback;
import system.wim.models.enums.FeedBackStatus;
import system.wim.models.modelsImpl.FeedbackImpl;

import java.util.List;
import java.util.NoSuchElementException;

public class CreateNewFeedbackInBoardCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse feedback parameters!";
    private final static String FEEDBACK_ALREADY_EXIST_MESSAGE = "Feedback %s already exist in %s board";
    private final static String BOARD_NOT_FOUND_MESSAGE = "Board not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public CreateNewFeedbackInBoardCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardName;
        String title;
        String description;
        int rating;
        FeedBackStatus status;

        try{
            boardName = parameters.get(0);
            title = parameters.get(1);
            description = parameters.get(2);
            rating = Integer.parseInt(parameters.get(3));
            status = FeedBackStatus.valueOf(parameters.get(4).toUpperCase());
        }catch (Exception e){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBoards().containsKey(boardName)){
            throw new NoSuchElementException(BOARD_NOT_FOUND_MESSAGE);
        }

        Board board = engine.getAllBoards().get(boardName);

        if(board.getWorkItems().stream().anyMatch(x->x.getTitle().equals(title))){
            throw new  IllegalArgumentException(String.format(FEEDBACK_ALREADY_EXIST_MESSAGE,title,boardName));
        }

        Feedback feedback = new FeedbackImpl(title,description,rating,status);

        board.addWorkItem(feedback);
        engine.getAllFeedbacks().put(title, feedback);
        String message =  String.format("A new feedback was created in %s",boardName);
        board.addToBoardActivityHistory(message);
        engine.getAllWorkItems().put(title,feedback);
        return message;
    }
}
