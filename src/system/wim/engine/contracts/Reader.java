package system.wim.engine.contracts;

public interface Reader {

    String readLine();

}
