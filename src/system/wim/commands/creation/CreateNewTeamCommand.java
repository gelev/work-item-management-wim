package system.wim.commands.creation;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Team;
import java.util.List;

public class CreateNewTeamCommand implements Command {
    private static final String TEAM_ALREADY_EXISTS_MESSAGE = "Team with name %s already exist";
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse team parameters";

    private final WorkItemFactory factory;
    private final Engine engine;

    public CreateNewTeamCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String name;

        try{
            name = parameters.get(0);
        }catch (Exception e){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (engine.getAllTeams().containsKey(name)){
            throw new IllegalArgumentException(String.format(TEAM_ALREADY_EXISTS_MESSAGE,name));
        }

        Team team = factory.createTeam(name);
        engine.getAllTeams().put(name,team);

        String message = String.format("Team with name \"%s\" was created.", name);
        team.addToTeamActivityHistory(message);
        return message;
    }
}
