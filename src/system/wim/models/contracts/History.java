package system.wim.models.contracts;

public interface History {

    String getChangedProperty();

    String getOldValue();

    String getNewValue();
}
