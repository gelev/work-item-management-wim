package system.wim.engine.providers;

import system.wim.engine.contracts.Writer;

public class ConsoleWriter implements Writer {
    @Override
    public void write(String message) {
        System.out.println(message);
    }

    @Override
    public void writeLine(String message) {
        System.out.println(message);
    }
}
