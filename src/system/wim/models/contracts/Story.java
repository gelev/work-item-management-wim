package system.wim.models.contracts;

import system.wim.models.enums.Priority;
import system.wim.models.enums.StorySize;
import system.wim.models.enums.StoryStatus;

public interface Story extends WorkItem, Task {
    StorySize getStorySize();

    StoryStatus getStoryStatus();

    void changeStoryPriority(Priority priority);

    void changeStorySize(StorySize size);

    void changeStoryStatus(StoryStatus status);
}
