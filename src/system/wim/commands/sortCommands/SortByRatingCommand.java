package system.wim.commands.sortCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Feedback;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SortByRatingCommand implements Command {
    private final WorkItemFactory factory;
    private final Engine engine;

    public SortByRatingCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        System.out.println("@@@@@@@@@@@@@@@@@@@@@");
        System.out.println("Sorting by rating:");
        List<Feedback> feedback = new ArrayList<>(engine.getAllFeedbacks().values());
        feedback.sort(Comparator.comparing(Feedback::getFeedbackRating));
        feedback.forEach(System.out::println);

        return "@@@@@@@@@@@@@@@@@@@@@";
    }
}
