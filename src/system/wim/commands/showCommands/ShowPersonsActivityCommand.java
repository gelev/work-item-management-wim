package system.wim.commands.showCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;

import java.util.List;
import java.util.NoSuchElementException;

public class ShowPersonsActivityCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse person's name parameter";
    private final static String NO_ACTIVITY_HISTORY_MESSAGE = "No activity history";
    private final static String NO_PERSON_MESSAGE = "No such person found";

    private final WorkItemFactory factory ;
    private final Engine engine;

    public ShowPersonsActivityCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String name;

        try{
            name=parameters.get(0);
        }catch (Exception ex){
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllMembers().containsKey(name)){
            throw new NoSuchElementException(NO_PERSON_MESSAGE);
        }

        if (engine.getAllMembers().get(name).getActivityHistory().isEmpty()){
            throw new IllegalArgumentException(NO_ACTIVITY_HISTORY_MESSAGE);
        }

        System.out.print(String.format("Person's (%s) activity history: ",name));
         engine.getAllMembers().get(name).getActivityHistory().forEach(x->System.out.print( " | " + x + " |"));
        return "";
    }
}
