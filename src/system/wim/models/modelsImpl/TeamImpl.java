package system.wim.models.modelsImpl;

import system.wim.models.contracts.Board;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.Team;

import java.util.*;
import java.util.stream.Collectors;

public class TeamImpl implements Team {
    private static final String MEMBER_ALREADY_EXISTS_IN_CURRENT_TEAM_MESSAGE = "%s already is in %s";
    private static final String NO_MEMBERS_FOUND = "No members in this team";

    private static final String BOARD_NAME_ALREADY_EXISTS_IN_CURRENT_TEAM_MESSAGE = "%s already exist!";

    private static final String TEAM_NAME_NULL_OR_EMPTY_MESSAGE = "Team name cannot be null or empty";

    private static final String NO_ACTIVITY_HISTORY_IN_TEAM_MESSAGE = "Team %s has no activity";
    private static final String ACTIVITY_NULL_OR_EMPTY_MESSAGE = "Activity cannot be null or empty";

    private String name;
    private Map<String, Member> members;
    private Map<String, Board> boards;
    private List<String> teamActivity;

    public TeamImpl(String name) {
        setName(name);
        members = new HashMap<>();
        boards = new HashMap<>();
        teamActivity = new ArrayList<>();
    }

    @Override
    public String showAllTeamMembers(){
        if (members.keySet().isEmpty()){
            throw new NoSuchElementException(NO_MEMBERS_FOUND);
        }
        return members.keySet().toString().replace("[","").replace("]","");
    }

    public String showTeamActivity(String teamName){
        if (teamActivity.isEmpty()){
            throw new IllegalArgumentException(String.format(NO_ACTIVITY_HISTORY_IN_TEAM_MESSAGE,getName()));
        }

        return String.join(" |;| ",teamActivity);
    }

    @Override
    public void addToTeamActivityHistory(String activity) {
        if (activity==null) {
            throw new IllegalArgumentException(ACTIVITY_NULL_OR_EMPTY_MESSAGE);
        }
        teamActivity.add(activity);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Map<String, Member> getMembers() {

        return members;
    }

    @Override
    public Map<String, Board> getBoards() {

        return boards;
    }

    @Override
    public void addMember(String memberName,Member member) {
       if (members.containsKey(memberName)){
           throw new IllegalArgumentException(String.format(MEMBER_ALREADY_EXISTS_IN_CURRENT_TEAM_MESSAGE,memberName,this.getName()));
       }
        members.put(memberName,member);
    }

    @Override
    public void addBoard(String boardName, Board board) {
        if (boards.containsKey(boardName)){
            throw new IllegalArgumentException(String.format(BOARD_NAME_ALREADY_EXISTS_IN_CURRENT_TEAM_MESSAGE,boardName));
        }
        boards.put(boardName,board);
    }

    private void setName(String name) {
        if (name==null || name.isEmpty()){
            throw new IllegalArgumentException(TEAM_NAME_NULL_OR_EMPTY_MESSAGE);
        }
        this.name = name;
    }
}
