package system.wim.commands.addCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.WorkItem;

import java.util.List;
import java.util.NoSuchElementException;

public class UnassignWorkItemToPersonCommand implements Command {
    private final static String MEMBER_DONT_HAVE_WORKITEM_MESSAGE = "Member %s is not assigned for this work item %s";
    private final static String WORK_ITEM_NOT_FOUND = "Work item not found";
    private final static String FAILED_TO_PARSE_PARAMETERS="[Error]: Failed to parse parameters while adding work item to person!";
    private final static String PERSON_NOT_FOUND = "Person not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public UnassignWorkItemToPersonCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String workItem;
        String personName;

        try {
            workItem = parameters.get(0);
            personName = parameters.get(1);

        } catch (Exception e) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllMembers().containsKey(personName)){
            throw new NoSuchElementException(PERSON_NOT_FOUND);
        }

        if (!engine.getAllWorkItems().containsKey(workItem)) {
            throw new NoSuchElementException(WORK_ITEM_NOT_FOUND);
        }

        WorkItem item = engine.getAllWorkItems().get(workItem);
        Member member = engine.getAllMembers().get(personName);

        if (!member.getWorkItems().contains(item)) {
            throw new IllegalArgumentException(String.format(MEMBER_DONT_HAVE_WORKITEM_MESSAGE,personName,workItem));
        }

        member.removeWorkItem(item);

        String message = String.format("Work item : %s unassigned from person %s", workItem, personName);
        member.addToActivityHistory(message);
        return message;
    }
}