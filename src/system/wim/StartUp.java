package system.wim;

import system.wim.engine.EngineImpl;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.engine.factory.WorkItemFactoryImpl;

public class StartUp {

    public static void main(String[] args) {
        WorkItemFactory factory = new WorkItemFactoryImpl();
        Engine engine = new EngineImpl(factory);
        engine.start();
    }
}
