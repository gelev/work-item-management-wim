package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.enums.BugSeverity;

import java.util.List;

public class ChangeSeverityOfBugCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse bug severity parameters!";
    private static final String BUG_NOT_FOUND = "Bug not found";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangeSeverityOfBugCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String bugName;
        BugSeverity newSeverity;

        try {
            bugName = parameters.get(0);
            newSeverity = BugSeverity.valueOf(parameters.get(1).toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBugs().containsKey(bugName)){
            throw new IllegalArgumentException(BUG_NOT_FOUND);
        }

        engine.getAllBugs().get(bugName).changeSeverityOfBug(newSeverity);
        return String.format("Severity type of Bug changed to : %s", newSeverity);
    }
}
