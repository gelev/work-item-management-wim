package system.wim.commands.filterCommand;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.contracts.Story;

import java.util.List;
import java.util.stream.Collectors;

public class FilterStoriesCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse board name parameter";
    private final static String NO_STORY_FOUND_MESSAGE = "No stories in this board";
    private final static String NO_BOARD_MESSAGE = "Board with name %s does not exist";

    private final WorkItemFactory factory;
    private final Engine engine;

    public FilterStoriesCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String boardName;

        try {
            boardName = parameters.get(0);
        }
        catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllBoards().containsKey(boardName))
        {
            throw new IllegalArgumentException(String.format(NO_BOARD_MESSAGE,boardName));
        }

        //FIND BETTER SOLUTION //TODO
         long count = engine.getAllBoards()
        .get(boardName)
        .getWorkItems()
        .stream()
        .filter(x -> x instanceof Story)
        .map(x -> (Story) x).count();

        if (count==0){
            throw new IllegalArgumentException(NO_STORY_FOUND_MESSAGE);
        }

        System.out.println("Filter stories:");
        return engine.getAllBoards()
                .get(boardName)
                .getWorkItems()
                .stream()
                .filter(x -> x instanceof Story)
                .map(x -> (Story) x)
                .collect(Collectors.toList())
                .toString()
                .replace("[","")
                .replace("]","");
    }
}
