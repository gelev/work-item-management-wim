package system.wim.commands.changeCommands;

import system.wim.commands.contracts.Command;
import system.wim.engine.contracts.Engine;
import system.wim.engine.factory.WorkItemFactory;
import system.wim.models.enums.StoryStatus;

import java.util.List;
import java.util.NoSuchElementException;

public class ChangeStatusOfStoryCommand implements Command {
    private static final String FAILED_TO_PARSE_PARAMETERS = "[ERROR]: Failed to parse story status parameters!";
    private static final String STORY_NOT_FOUND ="Story %s not found!";

    private final WorkItemFactory factory;
    private final Engine engine;

    public ChangeStatusOfStoryCommand(WorkItemFactory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String storyName;
        StoryStatus newStatus;

        try {
            storyName = parameters.get(0);
            newStatus = StoryStatus.valueOf(parameters.get(1).toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_PARAMETERS);
        }

        if (!engine.getAllStories().containsKey(storyName)){
            throw new NoSuchElementException(String.format(STORY_NOT_FOUND,storyName));
        }

        engine.getAllStories().get(storyName).changeStoryStatus(newStatus);
        return String.format("Status of story was changed to %s", newStatus);
    }
}

