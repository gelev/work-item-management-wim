package system.wim.models.modelsImpl;

import system.wim.models.contracts.Board;
import system.wim.models.contracts.WorkItem;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {
    private static final Integer BOARD_NAME_MIN_LENGTH =5;
    private static final Integer BOARD_NAME_MAX_LENGTH =10;

    private static final String BOARD_NAME_ERROR_MESSAGE = "The name of the board must be between %d and %d symbols!";
    private static final String EMPTY_NAME_MESSAGE = "Name cannot be null or empty!";

    private static final String EMPTY_COLLECTION_MESSAGE = "Work items cannot be null or empty!";
    private static final String ITEM_EXISTS_MESSAGE = "Work tem (%s) already exist";

    private String name;
    private List<WorkItem> workItems;
    private List<String> activityHistory;

    public BoardImpl(String name){
        setName(name);
        workItems=new ArrayList<>();
        activityHistory=new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<WorkItem> getWorkItems() {

        return workItems;
    }

    @Override
    public void addToBoardActivityHistory(String activity) {
        if (activity == null || activity.isEmpty()){
            throw new IllegalArgumentException(EMPTY_NAME_MESSAGE);
        }
        activityHistory.add(activity);
    }

    @Override
    public String getActivityHistory() {

        return  String.join(" | ",activityHistory);
    }

    @Override
    public void addWorkItem(WorkItem item) {
        if (item == null){
            throw new IllegalArgumentException(EMPTY_COLLECTION_MESSAGE);
        }
        if (workItems.contains(item)){
            throw new IllegalArgumentException(String.format(ITEM_EXISTS_MESSAGE,item.getTitle()));
        }
        workItems.add(item);
    }

    private void setName(String name) {
        if (name == null || name.isEmpty()){
            throw new IllegalArgumentException(EMPTY_NAME_MESSAGE);
        }

        if (name.length()<BOARD_NAME_MIN_LENGTH || name.length()>BOARD_NAME_MAX_LENGTH){
            throw new IllegalArgumentException(String.format(BOARD_NAME_ERROR_MESSAGE,BOARD_NAME_MIN_LENGTH,BOARD_NAME_MAX_LENGTH));
        }
        this.name = name;
    }
}
