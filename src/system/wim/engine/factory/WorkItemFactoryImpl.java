package system.wim.engine.factory;

import system.wim.models.modelsImpl.BoardImpl;
import system.wim.models.modelsImpl.MemberImpl;
import system.wim.models.modelsImpl.TeamImpl;
import system.wim.models.contracts.Board;
import system.wim.models.contracts.Member;
import system.wim.models.contracts.Team;

public class WorkItemFactoryImpl implements WorkItemFactory {
    public WorkItemFactoryImpl(){}

    @Override
    public Team createTeam(String name) {
        return new TeamImpl(name);
    }

    @Override
    public Member createMember(String name) {
        return new MemberImpl(name);
    }

    @Override
    public Board createBoard(String name) {
        return new BoardImpl(name);
    }

}
