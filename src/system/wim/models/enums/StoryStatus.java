package system.wim.models.enums;

public enum StoryStatus {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this)
        {
            case DONE:
                return "Done";
            case NOTDONE:
                return "NotDone";
            case INPROGRESS:
                return "InProgress";
                default:
                    throw new IllegalArgumentException("The status of the story is invalid!");
        }
    }
}
