package system.wim.models.contracts;
import java.util.Map;

public interface Team {
    String getName();

    String showTeamActivity(String teamName);

    void addToTeamActivityHistory(String activity);

    String showAllTeamMembers();

    Map<String, Member> getMembers();

    Map<String, Board> getBoards();

    void addMember(String memberName, Member name);

    void addBoard(String boardName, Board board);
}
