package system.wim.models.contracts;

public interface WorkItem {
    Integer getId();

    String getTitle();

    String getDescription();

    String getComments();

    String getHistory();

    void addComment(Comment comment);

    void addHistory(History history);

}
