package system.wim.models.enums;

public enum FeedBackStatus {
    NEW,
    SCHEDULED,
    UNSCHEDULED,
    DONE;

    @Override
    public String toString() {
        switch (this){
            case DONE:
                return "Done";
            case NEW:
                return "New";
            case SCHEDULED:
                return "Scheduled";
            case UNSCHEDULED:
                return "Unscheduled";

                default:
                    throw new IllegalArgumentException("Invalid Feedback status!");
        }
    }
}
