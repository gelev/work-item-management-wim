package system.wim.models.contracts;

import system.wim.models.enums.FeedBackStatus;

public interface Feedback extends WorkItem {
    int getFeedbackRating();

    FeedBackStatus getFeedbackStatus();

    void changeFeedbackStatus(FeedBackStatus status);

    void changeFeedbackRating(int rating);
}
